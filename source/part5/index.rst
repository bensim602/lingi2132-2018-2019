.. _part5:

*************************************************************************************************
Partie 5 | Garbage Collection
*************************************************************************************************

Cheney algorithm proposed by Group 19, Arthur Sluyters and Benjamin Simon
==========================================================================

The Cheney algorithm uses the Forward function. 
Give its pseudo code and explain the cases that it has to handle.

Answer
"""""""
.. code-block:: java

	function Forward(p) {
		if (p points to from-space) {
			if (p.f1 points to to-space) { 
				return p.f1 //already moved: we just need to change the pointer
				
			} else {
				//we have to moved the object from from-space to to-space
				
				for (field fi : p) {
					next.fi = p.fi
				}
				p.f1 = next
				next += size of record p
				return p.f1
			}
		} else {
			return p //the job has already been done: nothing to do
			
		}
	}

Question 2
"""""""""""

Apply the algorithm to the given heap. Explain briefly each step.
	.. image:: img/cheney1.svg
		:alt: Question 2
		:height: 400

Answer
"""""""
	.. image:: img/cheney2.svg
		:alt: Answer2 Part 1
		
	.. image:: img/cheney3.svg
		:alt: Answer2 Part 2