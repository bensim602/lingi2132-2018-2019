.. _part3:

*************************************************************************************************
Partie 3 | Semantic Analysis
*************************************************************************************************

Questions proposed by Group 27, Jacques Yakoub & Alexandre Dewit
================================================================

1. Using a code sample, explain how can we handle context with context objects
------------------------------------------------------------------------------

2. Explain what is variable shadowing and how can we detect it 
--------------------------------------------------------------


Answers
"""""""

1. Using this example : 

.. literalinclude:: Factorial.java
    :linenos:
    :language: java

* CompilationUnitContext on lines 1-16.
* ClassContext on lines 3-16.
* MethodContext on lines 5-8 and 10-13.

At any point of execution, we have a stack of Context's.
That is useful for example if we lookup for the definition of a variable (like the 'n' in the code sample).

+------------------------+
| CompilationUnitContext |
+------------------------+
| ClassContext           |
+------------------------+
| MethodContext          |
+------------------------+
| LocalContext1          |
+------------------------+
| LocalContext2          |
+------------------------+


2. 

Definition from `Wikipedia <https://en.wikipedia.org/wiki/Variable_shadowing>`_ : 
Variable shadowing occurs when a variable declared within a certain scope has the same name as a variable declared in an outer scope

For the detection part, check the course slides at page 36.


Questions proposed by Group 3, Nour-Eddine Sabbani & Simon Ponchau
==================================================================


1. Return of a program
-----------------------

String test="hello";
if (true) {
	String test="world";
}

What will this program generate? How is it called?
Can you explain why this type of output takes place?


2. Contexts
-----------------------

package lt.fib;
class Fibonacci{ 

	public static String boucle(int n1, int n2) {
		int i, n3;
		int count=10;
		String myReturn="";
		for(i=2;i<count;++i)   
		{    
			n3=n1+n2;    
			myReturn+=" "+n3;    
			n1=n2;    
			n2=n3;    
		}  
		return myReturn;
	}

	public static void main(String args[])  
	{    
		int n1=0,n2=1;    
		System.out.print(n1+" "+n2+boucle(n1, n2));

	}
} 


List the different types of contexts in this program. Delimit them.
Give the symbol table of these.

3. Stack Frame
-----------------------

public class GiveMeThePointPlease
{
	
	public int result(int t, int[] u)
	{
		int totalResult = 0 ;
		{
			int resultTest1 = totalResult + u[1] ;
			int resultTest2 = u[2] + resultTest1 ;
			totalResult = resultTest2 / 2 ;
		}
		{
			int freePoint = 3;
			int z = freePoint / 2 ;
			t = z+1;
		}
		return t + totalResult;
	}
}

Give the stack Frame of GiveMeThePointPlease invocation.